

var field1 = document.getElementById("field1");
var field2 = document.getElementById("field2");
var field3 = document.getElementById("field3");
var field4 = document.getElementById("field4");
var field5 = document.getElementById("field5");
var field6 = document.getElementById("field6");
var btnLosuj = document.getElementById("btnRandom");

btnLosuj.addEventListener('click', function(e){
    //blokuje domyslne dzialanie klikniecia
    e.preventDefault();
    var values = getInputsValues();

    if(checkIsEmpty(values) && checkIsNumber(values)){
            alert("numbers are correct");
            if(checkIsInScope(values) && checkIsRedunant(values)){
                schuffe();
            }
    }
    else{
        console.log("not correct");
    }
});

function getInputsValues( ){
    return [field1.value, field2.value, field3.value, field4.value, field5.value, field6.value];
}

function checkIsEmpty(values){
    for(var i=0; i<values.length; i++){
        var element = values[i];
        if(typeof element==='undefined' || element===null || element===""){
            return  false;
        }
    }
    return true;
}

function checkIsNumber(values){
    for(var i=0; i<values.length; i++){
        var element = values[i];
        if(isNaN(element) ){
            return  false;
        }
    }
    return true;
}


//&& element === parseInt(element,10)
function checkIsInScope(values){
    for(var i=0; i<values.length; i++){
        if(values[i]<1 || values[i]> 49){
            return false;
        }
    }
    return true;
}

function checkIsRedunant(values){
    for(var i =0 ; i<values.length; i++){
        for(var j=0; j< values.length ; j++){
            if(i !== j && values[i] === values[j] ){
                return false;
            }
        }
    }
    return true;
}

function schuffe(){
    var shuffledDigits = [];
    while(shuffledDigits.length<6){
        var digit = Math.floor(Math.random()* 48 +1);
        if(shuffledDigits.indexOf(digit) === -1){
            shuffledDigits.push(digit);
        }
    }
    return shuffledDigits;
}

function checkHits(values, shuffledDigits){

    var hits = [];
    for(var i=0; i<values.length; i++){
        for(var j=0; j<shuffledDigits.length; j++){
            if(shuffledDigits[j] === values[i]){
                hits.push(values[i]);
            }
        }
    }
    return hits;
}

function showResults(){

}